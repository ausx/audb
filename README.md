# audb

#### 介绍
一个数据高度加密，具有反DDOS，反破解机制的数据库

#### 软件架构
audb适用于复杂指令集arrch64，x86，精简指令集arm，arm64，risc-v等支持Pythonpython，不限系统
(能正确执行下方安装教程的步骤就算支持)
#### 安装教程
##### Windows
1.  安装好Python3和pip，一定要添加path
2.  cmd cd到目录并输入pip install pycryptodomex安装库并安装编译器pip install pyinstaller
3.  编译：pyinstaller main.spec
##### linux/Android
1.  安装Python3
2.  cd到目录，剩下的与Windows操作一样，pycryptodomex去掉x
！！注意：android没有安装包，需要将linux的复制到android然后利用网络上的软件打开终端，cd到目录，执行。
！！cython+msvc编译发行版已经上线
### 截图
暂无