#自制高速文件缓存打开技术
import os,sys,json,base64,threading
#secret=False #根据内存速度与大小决定
thread=4
_cache={}
_w_list=[]
def open_f(file=str):
    global _cache
    t=''
    if file in _cache :
        return _cache[file]
    if os.path.isfile(file):
        with open(file,'r') as f:
            t=f.read()
            _cache.setdefault(file,t)
    return t
def write(file=str,t=str):
    global _cache,_w_list
    if file in _cache:
        _cache[file]=t
        
    else:
        _cache.setdefault(file,t)
    if file in _w_list:
        None
    else:
        _w_list.append(file)
def delete(file):
    global _cache
    if file in _cache:
        del _cache[file]
    if file in _w_list:
        _w_list.remove(file)
    os.remove(file)
def _write_list():
    global _cache,_w_list
    while True:
        if not len(_w_list)==0:
            b=_w_list.pop()
            try:
                with open(b,'w' ) as f:
                    f.write(_cache[b])
            except:
                _w_list.append(b)
for i in range(thread):
    t=threading.Thread(target=_write_list,args=())
    t.daemon=True
    t.start()

