import json
import file
import os
import base64
import hashlib
def readconf(conf):
    if os.path.isfile(conf)==False:
        return {}
    return json.loads(base64.b64decode(file.open_f(conf)).decode('utf-8'))
def writeconf(conf,code):
    file.write(conf,base64.b64encode(bytes(json.dumps(code).encode('utf-8'))).decode('utf-8'))
def init():
    if os.path.isfile('data/user.conf')==False:
        writeconf('data/user.conf',{'user':[{'name':'root','key':'fFgjS4-','pwd':'15-9DbAvh'}]})
def createuser(name,key,pwd,size=1204,mustlocal=False):
    for i in readconf('data/user.conf')['user']:
        if i['name']==name:
            return False
    conf=readconf('data/user.conf')
    conf['user'].append({'name':name,'key':key,'pwd':pwd,'size':size,'mustlocal':mustlocal})
    writeconf('data/user.conf',conf)
    return True
def deluser(name):
    conf=readconf('data/user.conf')
    for i in conf['user']:
        if i['name']==name:
            conf['user'].remove(i)
            writeconf('data/user.conf',conf)
            return True
    return False
def login(name,key,pwd):
    for i in readconf('data/user.conf')['user']:
        if i=={'name':name,'key':key,'pwd':pwd}:
            return True
    return False
def check(sign):
    for i in readconf('data/user.conf')['user']:
        if sign==hashlib.sha3_512(json.dumps(i)):
            return True
    return False
def checkuser(name):
    for i in readconf('data/user.conf')['user']:
        if i['name']==name:
            return True
    return False
def checkdb(name,db):
    if os.path.isdir('data/'+name+'/')==False:
        os.mkdir('data/'+name)
    if os.path.isfile('data/'+name+'/db.json')==False:
        writeconf('data/'+name+'/db.json',{'list':[]})
        return False
    for i in readconf('data/'+name+'/db.json')['list']:
        if i==name:
            if os.path.isdir('data/'+name+'/'+db)==False:
                os.mkdir('data/'+name+'/'+db)
            return True
def checktable(name,db,table):
    if checkdb(name,db):
        if os.path.isdir('data/'+name+'/'+db+'/'+table)==False:
            os.mkdir('data/'+name+'/'+db+'/'+table)
        if os.path.isfile('data/'+name+'/'+db+'/table.json')==False:
            writeconf('data/'+name+'/'+db+'/table.json',{'list':[]})
            return False
        for i in readconf('data/'+name+'/'+db+'table.json')['list']:
            if i==name:
                return True
def createdb(name,db):
    if checkdb(name,db):
        return False
    conf=readconf('data/'+name+'/db.json')
    conf['list'].append(db)
    writeconf('data/'+name+'/db.json',conf)
    if os.path.isdir('data/'+name+'/'+db)==False:
        os.mkdir('data/'+name+'/'+db)
    return True
def signisroot(sign):
    for i in readconf('data/user.conf')['user']:
        if sign==hashlib.sha3_512(json.dumps(i)) and i['name']=='root':
            return True
    return False
def deldb(name,db):
    if checkdb(name,db):
        return 
    conf=readconf('data/'+name+'/db.json')
    conf['list'].remove(db)
    writeconf('data/'+name+'/db.json',conf)
    if os.path.isdir('data/'+name+'/'+db)==False:
        os.removedirs('data/'+name+'/'+db)
    return True
def createtable(name,db,table,type=[]):
    if checkuser(name) and checkdb(name,db) and checktable(name,db,table)==False:
        conf=readconf('data/'+name+'/'+db+'/table.json')
        conf['list'].append(table)
        conf=writeconf('data/'+name+'/'+db+'/table.json',conf)
        writeconf('data/'+name+'/'+db+'/'+hashlib.sha3_384(table),{'list':[type]})
        return True
    else:
        return False
def deltable(name,db,table):
    if checkuser(name) and checkdb(name,db) and checktable(name,db,table):
        conf=readconf('data/'+name+'/'+db+'/table.json')
        conf['list'].remove(table)
        conf=writeconf('data/'+name+'/'+db+'/table.json',conf)
        if os.path.isfile('data/'+name+'/'+db+'/'+hashlib.sha3_384(table))==False:
            file.delete('data/'+name+'/'+db+'/'+hashlib.sha3_384(table))
        return True
    return False
def checkvalue(type,t):
    if len(type)!=len(t):
        return False
    for i,i2 in type,t:
        if type(i2)== int and i=='int':
            None
        elif type(i2)== str and i=='str':
            None
        if type(i2)== list and i=='list':
            None
        if type(i2)== dict and i=='dict':
            None
        else:
            return False
    return True
def getusersize(name):
    for i in readconf('data/user.conf')['user']:
        if 'size' in i and i['name']==name:
            return i['size']
    return 0
def foldersize(path):
    # 获取文件夹内所有文件名称
    listdirs = os.listdir(path) 
    big = 0
    for listdir in listdirs:
        # 判断是不是文件夹
        if not( os.path.isfile(f'{path}\{listdir}')):
            big += foldersize(f'{path}\\{listdir}')
        else:
            # 获得文件大小
            size = os.path.getsize(f'{path}\{listdir}')
            big += size
        
    return big
def addlinetotable(name,db,table,text):
    if foldersize('data/'+name)+text*256<getusersize(name):
        if checkuser(name) and checkdb(name,db) and checktable(name,db,table):
            conf=readconf('data/'+name+'/'+db+'/'+hashlib.sha3_384(table))

            conf['list'].append(table)
            conf=writeconf('data/'+name+'/'+db+'/'+hashlib.sha3_384(table))
#print(readconf('data/user.conf'))
def mustlocal(name):
    if checkuser(name):
        for i in readconf('data/user.conf')['user']:
            if 'mustlocal' in i and i['name']==name:
                return i['mustlocal']
        return False
    else:
        return False
def getnamefromsign(sign):
    for i in readconf('data/user.conf')['user']:
        if hashlib.sha3_512(i['name']+i['key']+i['pwd'])==sign:
            return i['name']
    return ''