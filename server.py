import socket
import threading
import socketserver
import time
import json
import base64
import rsaop
import user
noDDOSandHarker = []
conf={}

class TCPRequestHandler(socketserver.BaseRequestHandler):  # 服务器接收函数

    def handle(self):
        addr = self.client_address
        global noDDOSandHarker
        global conf
        if True:
            if conf['anti_ddos'] and len(noDDOSandHarker) == conf['anti_ddos_list_up']:
                noddos = 0
                for i in noDDOSandHarker:
                    if i['ip'] == addr[0]:
                        noddos += 1
                if noddos == len(noDDOSandHarker) and time.localtime().tm_year == noDDOSandHarker[0]['time'].tm_mon and time.localtime().tm_mon == noDDOSandHarker[0]['time'].tm_mon and time.localtime().tm_yday == noDDOSandHarker[0]['time'].tm_yday and time.localtime().tm_min == noDDOSandHarker[0]['time'].tm_min and time.localtime().tm_sec - noDDOSandHarker[0]['time'].tm_src <= 1.2:
                    noDDOSandHarker.pop(0)
                    noDDOSandHarker.append(
                        {'ip': addr[0], 'time': time.localtime()})
                    if conf['writelog']:
                        with open('data/audb.log', 'a') as file:
                            file.write('['+str(time.localtime()) +
                                    ']\n maybe harker or DDOS'+str(addr[0]))
                    return ''
                noDDOSandHarker.pop(0)
                noDDOSandHarker.append(
                    {'ip': addr[0], 'time': time.localtime()})
            else:
                noDDOSandHarker.append(
                    {'ip': addr[0], 'time': time.localtime()})
            data = str(self.request.recv(1024*1024*1024), 'ascii')
            response = base64.b64decode(str(data))
            re = solve(response, self.client_address)
            rm=re['msg']
            if not re['rsakey']=='':
                try:
                    re['msg']=rsaop.encrypt(re['rsakey'],re['msg'])
                except:re['msg']=rm
            self.request.sendall(rsaop.encrypt(re['rsakey'],base64.b64encode(json.dumps(
                re['msg'])).encode('utf-8')))
            if conf['writelog']:
                with open('data/audb.log', 'a') as file:
                    file.write('['+str(time.localtime()) + ']\n receive data from'+str(self.client_address[0])+'\n')
        else:
            if conf['writelog']:
                with open('data/audb.log', 'a') as file:
                    file.write('['+str(time.localtime()) +
                            ']\n receive data of rubbish from'+str(self.client_address[0])+'\n')


class TCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):  # 服务器
    pass


def client(ip, port, message):  # 客户端
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((ip, port))
        sock.sendall(bytes(message, 'ascii'))
        response = str(sock.recv(1024), 'ascii')
        return response


def init(config):
    global conf
    conf=config
    try:
        HOST, PORT = '0.0.0.0', config['Server']['port']#socket.gethostbyname(socket.gethostname())
        server = TCPServer((HOST, PORT), TCPRequestHandler)
        ip, port = server.server_address
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
    except:
        if input('error:cannot start socket.try again?(y/n)') == 'y':
            print('restart socket!')
            init(conf)
    #server.shutdown()


def solve(response, addr):
    re,code,status,rsakey='',200,True,'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDqmq6vPKBX216D6lCPetO80mSr\nVBZ+TBzblmnzyLfmBCkRfFd/JwEHdyrFUYSv6OmlK72cVanC0O6hoq3jqZ3J3GnL\n30/UTTlSIqX2NySC57xcJHLxPHOn731cl/2OBWOoLg2ASkpy+RpuLMPYq9F7ui6g\nohMB4X83UNG5rER5mwIDAQAB'
    try:
        rsakey=base64.b64decode(response[base64.b64encode('rsakey')])
        recv = json.loads(response)
        rsakey = recv['rsakey']
        if user.mustlocal(base64.a85decode(response['name'])):
             return {'msg': {'msg': re, 'code': code, 'status': status,'ip':addr[0]}, 'rsakey': rsakey}
        elif response['op']=='login':
            name,key,pwd = base64.a85decode(response['name']),base64.a85decode(response['key']),base64.a85decode(response['pwd'])
            status=user.login(name,key,pwd)
        elif response['op']=='deluser':
            sign,name=base64.a85decode(response['sign']),base64.a85decode(response['name'])
            if user.signisroot(sign):
                status=user.deluser(name)
            else:
                status=False
                code=403
        elif response['op']=='createuser':
            sign,name,key,pwd=base64.a85decode(response['sign']),base64.a85decode(response['name']),base64.a85decode(response['key']),base64.a85decode(response['pwd'])
            if user.signisroot(sign):
                status=user.createuser(name,key,pwd)
            else:
                status=False
                code=403
        elif response['op']=='getalldb':
             sign=response['sign']
             if user.getnamefromsign(sign)!='':
                    re={'dblist':user.getalldb(user.getnamefromsign(sign))}
             else:
                    status=False
                    code=403
        elif response['op']=='getalltable':
             sign,db=response['sign'],response['db']
             if user.getnamefromsign(sign)!='':
                    re={'tablelist':user.getalltable(user.getnamefromsign(sign))}
             else:
                    status=False
                    code=403
        elif response['op']=='getalldatafromtable':
             sign,db,table=response['sign'],response['db'],response['table']
             if user.getnamefromsign(sign)!='':
                    re={'datalist':user.getalldatafromtable(user.getnamefromsign(sign),db,table)}
                    
             else:
                    status=False
                    code=403
        elif response['op']=='deldb':
            sign,db=response['sign'],response['db']
            if user.getnamefromsign(sign)!='':
                status=user.deldb(user.getnamefromsign(sign),db)
            else:
                code=403
                status=False
        elif response['op']=='deltable':
            sign,db,table=response['sign'],response['db'],response['table']
            if user.getnamefromsign(sign)!='':
                status=user.deltable(user.getnamefromsign(sign),db,table)
            else:
                code=403
                status=False
        elif response['op']=='addlinetotable':
            sign,db,table,line=response['sign'],response['db'],response['table'],response['line']
            if user.getnamefromsign(sign)!='':
                status=user.addlinetotable(user.getnamefromsign(sign),db,table,line)
            else:
                code=403
                status=False
        elif response['op']=='removelinefromtable':
            sign,db,table,line=response['sign'],response['db'],response['table'],response['line']
            if user.removelinefromtable(sign)!='':
                status=user.removelinefromtable(user.getnamefromsign(sign),db,table,line)
            else:
                code=403
                status=False
    except:
        re = ''
        code = 502
        status = False
    return {'msg': {'msg': re, 'code': code, 'status': status,'ip':addr[0]}, 'rsakey': rsakey}
